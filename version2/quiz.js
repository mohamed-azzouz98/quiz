const quizData = [
[
	{
	theme: 'SNK
	
	},
    {
        question: 'Qui est l\'auteur de SnK ?',
        a: 'Tôru Fujisawa',
        b: 'Asime Isayama',
        c: 'Hajime Isayama',
        d: 'Miki Yoshikawa',
        correct: 'c',
    },
    {
        question: 'Quelle est la position d\'Eren dans le classement des recrues de la 104ème Brigade d\'Entrainement ?',
        a: '1er',
        b: '3eme',
        c: '4eme',
        d: '5eme',
        correct: 'd',
    },
    {
        question: 'Sous quel faux nom Eren s\'infiltre-t\'il à Revelio ?',
        a: 'M.Kruger',
        b: 'M.Kruler',
        c: 'M.Krumer',
        d: 'M.Kruder',
        correct: 'a',
    },
    {
        question: 'Quel est le point faible des Titans ?',
        a: 'Les Yeux',
        b: 'Les Pieds',
        c: 'La Nuque',
        d: 'Les Oreilles',
        correct: 'c',
    },
    {
        question: 'Quel objet, le père d’Eren a donné à son fils avant de disparaître ?',
        a: 'Un collier',
        b: 'Une montre',
        c: 'Un carnet de notes.',
        d: 'La clé du sous-sol de leur maison.',
        correct: 'd',
    },


],
[
{
            theme: 'DBZ'
        },
        {
            question: 'Par qui Goku a-t-il déjà été tué ?',
            a: 'Vegeta',
            b: 'Cell',
            c: 'Buu',
            d: 'Beerus',
            correct: 'b',
        },
        {
            question: 'Quel est l\'autre nom de Son Goku ?',
            a: 'Kakarot',
            b: 'Son Goten',
            c: 'Neiru',
            d: 'Kami',
            correct: 'a',
        },
        {
            question: 'Quel est le résultat de la fusion entre Son Goku et Végéta grâce au potalas ?',
            a: 'Gotrunks',
            b: 'Gogeta',
            c: '	Vegeto',
            d: 'Sangéta',
            correct: 'c',
        },
        {
            question: 'Qui est responsable de la destruction de la planète Végéta ?',
            a: 'Freezer',
            b: 'Chichi',
            c: 'Végéta',
            d: 'Broly',
            correct: 'a',
        },
        {
            question: 'Combien faut il de boule de cristal pour inoquer Shenron ?',
            a: '2',
            b: '4',
            c: '5',
            d: '7',
            correct: 'd',
        },
    ]




]






const quizz = document.querySelector('#quizContainer')

const quizHeader = document.querySelector('#quizHeader')

const themes = document.querySelector('#themes')

const question = document.querySelector('#question');

const a_text = document.querySelector('#a_text')
const b_text = document.querySelector('#b_text')
const c_text = document.querySelector('#c_text')
const d_text = document.querySelector('#d_text')

const submitBtn = document.querySelector('#submit')

const error = document.querySelector('#error')

const answerEls = document.querySelectorAll('input[name="answer"]')



let currentQuiz = 1;
let score = 0;



for (let i = 0; i < quizData.length; i++) {
    listTheme = document.createElement('li')
    buttonTheme = document.createElement('button')
    buttonTheme.classList.add('buttonTheme')
    buttonTheme.setAttribute('id', 'theme' + i)


    themes.append(listTheme);
    listTheme.append(buttonTheme)
    buttonTheme.append(quizData[i][0].theme)



   


    const selectTheme = document.querySelector('#theme' + i)


    selectTheme.addEventListener('click', () => {
        themes.style.display = 'none'
        quizHeader.style.display = 'block'
        for (let y = 0; y <= quizData[i].length; y++) {
            quiz();


            function quiz() {
                deselectAnswer()

                const currentQuizData = quizData[i][currentQuiz]
                question.innerText = currentQuizData.question
                a_text.innerText = currentQuizData.a
                b_text.innerText = currentQuizData.b
                c_text.innerText = currentQuizData.c
                d_text.innerText = currentQuizData.d
            }


            function getSelected() {
                let answer = undefined;

                answerEls.forEach((answerEl) => {
                    if (answerEl.checked) {
                        answer = answerEl.id
                    }
                })

                return answer
            }

            function deselectAnswer() {
                answerEls.forEach((answerEl) => {
                    answerEl.checked = false
                })
            }

            submitBtn.addEventListener('click', () => {


                const answer = getSelected()

                if (answer) {
                    if (answer === quizData[i][currentQuiz].correct) {
                        score++
                    }
                    currentQuiz++

                    if (currentQuiz < quizData[i].length) {
                        quiz()
                    }
                    else {
                        quizz.innerHTML =
                            `<h2>Ton score est de ${score} / ${quizData[i].length - 1}</h2>
                            <button onclick="location.reload()" id="reload">Reload</button>`
                    }
                }
            })
        }
    })
}






