const quizData = [

    {
        question: 'Qui est l\'auteur de SnK ?',
        a: 'Tôru Fujisawa',
        b: 'Asime Isayama',
        c: 'Hajime Isayama',
        d: 'Miki Yoshikawa',
        correct: 'c',
    },
    {
        question: 'Quelle est la position d\'Eren dans le classement des recrues de la 104ème Brigade d\'Entrainement ?',
        a: '1er',
        b: '3eme',
        c: '4eme',
        d: '5eme',
        correct: 'd',
    },
    {
        question: 'Sous quel faux nom Eren s\'infiltre-t\'il à Revelio ?',
        a: 'M.Kruger',
        b: 'M.Kruler',
        c: 'M.Krumer',
        d: 'M.Kruder',
        correct: 'a',
    },
    {
        question: 'Quel est le point faible des Titans ?',
        a: 'Les Yeux',
        b: 'Les Pieds',
        c: 'La Nuque',
        d: 'Les Oreilles',
        correct: 'c',
    },
    {
        question: 'Quel objet, le père d’Eren a donné à son fils avant de disparaître ?',
        a: 'Un collier',
        b: 'Une montre',
        c: 'Un carnet de notes.',
        d: 'La clé du sous-sol de leur maison.',
        correct: 'd',
    },


]







const quizz = document.querySelector('#quizContainer')



const question = document.querySelector('#question');

const a_text = document.querySelector('#a_text')
const b_text = document.querySelector('#b_text')
const c_text = document.querySelector('#c_text')
const d_text = document.querySelector('#d_text')

const submitBtn = document.querySelector('#submit')

const error = document.querySelector('#error')

const answerEls = document.querySelectorAll('input[name="answer"]')

let currentQuiz = 0;
let score = 0;







quiz();

function quiz() {
    deselectAnswer()

    const currentQuizData = quizData[currentQuiz]
    question.innerText = currentQuizData.question
    a_text.innerText = currentQuizData.a
    b_text.innerText = currentQuizData.b
    c_text.innerText = currentQuizData.c
    d_text.innerText = currentQuizData.d
}


function getSelected() {
    let answer = undefined;

    answerEls.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id
        }
    })

    return answer
}

function deselectAnswer() {
    answerEls.forEach((answerEl) => {
        answerEl.checked = false
    })
}

submitBtn.addEventListener('click', () => {


    const answer = getSelected()

    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++
            console.log('score = ' + score);
        }
        else {

        }

        currentQuiz++
        if (currentQuiz < quizData.length) {
            quiz()
        }
        else {
            quizz.innerHTML =
                `<h2>Ton score est de ${score} / ${quizData.length}</h2>
                <button onclick="location.reload()">Reload</button>`
        }
    }

})



